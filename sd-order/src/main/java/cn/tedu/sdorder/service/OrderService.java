package cn.tedu.sdorder.service;

import cn.tedu.sdorder.entity.Order;

public interface OrderService {
    void create(Order order);
}
